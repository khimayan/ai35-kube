import cv2
import numpy as np
from PIL import Image, ImageEnhance, ImageFile
import kociemba


def extraire_couleurs_pures(image_path):
    # Charger l'image
    image = Image.open(image_path)
    image = image.convert('RGBA')

    # Dictionnaire pour stocker le premier pixel de chaque couleur
    premier_pixels = {
        "rouge": None,
        "vert": None,
        "bleu": None,
        "jaune": None,
        "magenta": None,
        "cyan": None,
        "blanc": None,
        "noir": None,
        "gris": None
    }

    # Parcourir chaque pixel
    for x in range(image.width):
        for y in range(image.height):
            r, g, b, a = image.getpixel((x, y))
            if a == 0:
                continue

            # Vérifier et stocker le premier pixel de chaque couleur
            if r == 255 and g == 0 and b == 0 and premier_pixels["rouge"] is None:
                premier_pixels["rouge"] = [x, y]
            elif r == 0 and g == 255 and b == 0 and premier_pixels["vert"] is None:
                premier_pixels["vert"] = [x, y]
            elif r == 0 and g == 0 and b == 255 and premier_pixels["bleu"] is None:
                premier_pixels["bleu"] = [x, y]
            elif r == 255 and g == 255 and b == 0 and premier_pixels["jaune"] is None:
                premier_pixels["jaune"] = [x, y]
            elif r == 255 and g == 0 and b == 255 and premier_pixels["magenta"] is None:
                premier_pixels["magenta"] = [x, y]
            elif r == 0 and g == 255 and b == 255 and premier_pixels["cyan"] is None:
                premier_pixels["cyan"] = [x, y]
            elif r == 255 and g == 255 and b == 255 and premier_pixels["blanc"] is None:
                premier_pixels["blanc"] = [x, y]
            elif r == 0 and g == 0 and b == 0 and premier_pixels["noir"] is None:
                premier_pixels["noir"] = [x, y]
            elif r == 128 and g == 128 and b == 128 and premier_pixels["gris"] is None:
                premier_pixels["gris"] = [x, y]

    # Assembler les pixels dans l'ordre spécifié
    pixels_ordonnes = [premier_pixels[couleur] for couleur in premier_pixels if premier_pixels[couleur] is not None]

    return pixels_ordonnes


rgbValues = [[255,0,0],[0,255,0],[0,0,255],[255,150,0],[255,100,0],[255,255,255]]
rgbColors= ['red'      ,'green',   'blue',   'yellow',   'orange',    'white']

def findNearest(rgb):
    dist = ((rgbValues[0][0]-rgb[0]))**2 + ((rgbValues[0][1]-rgb[1]))**2 + ((rgbValues[0][2]-rgb[2]))**2
    index = 0
    for i in range(1,len(rgbValues)):
        d = ((rgbValues[i][0]-rgb[0]))**2 + ((rgbValues[i][1]-rgb[1]))**2 + ((rgbValues[i][2]-rgb[2]))**2
        if d < dist:
            dist = d
            index = i
    return rgbColors[index]


def get_pixel_color(image_path, x, y):
    """
    Lit la couleur RGB d'un pixel dans une image.

    :param image_path: Chemin vers l'image.
    :param x: Coordonnée X du pixel.
    :param y: Coordonnée Y du pixel.
    :return: Tuple RGB représentant la couleur du pixel.
    """
    with Image.open(image_path) as img:
        rgb_color = img.getpixel((x, y))
    return rgb_color


def increase_saturation(image_path, enhancement_factor):
    """
    Augmente la saturation d'une image.

    :param image_path: Chemin vers l'image.
    :param enhancement_factor: Facteur d'augmentation de la saturation (>1 pour augmenter).
    :return: Image avec saturation augmentée.
    """
    # Ouvrir l'image
    try:
        with Image.open(image_path) as img:
            # Ignorer les erreurs de fichiers tronqués
            ImageFile.LOAD_TRUNCATED_IMAGES = True

            # Créer un objet d'amélioration de la couleur
            enhancer = ImageEnhance.Color(img)
            # Augmenter la saturation
            img_enhanced = enhancer.enhance(enhancement_factor)
            return img_enhanced
    except IOError:
        print("Erreur lors de l'ouverture de l'image.")


def set_brightness(image_path, factor):
    """
    Diminue la luminosité d'une image.

    :param image_path: Chemin vers l'image.
    :param factor: Facteur de diminution de la luminosité (0 < factor < 1 pour diminuer).
    :return: Image avec luminosité diminuée.
    """
    with Image.open(image_path) as img:
        enhancer = ImageEnhance.Brightness(img)
        img_enhanced = enhancer.enhance(factor)
        return img_enhanced

def solve(faceTop, faceRight, faceFront, faceDown, faceLeft, faceBack): 
    
    brightness_values = [1, 1.5, 1.2, 1.2, 2, 2]
    saturation_values = [3, 2, 3, 2, 1, 2]

    image_path = ['saved_photo_cam1/saved_photo_cam1.jpg', 'saved_photo_cam1/saved_photo_cam2.jpg']

    faces = np.empty((6,0)).tolist()
    for i in range(6):
        current_image = image_path[i//3]
        enhanced_image = increase_saturation(current_image, saturation_values[i])
        enhanced_image.save('saved_photo_cam_enhanced.jpg')
        enhanced_image = set_brightness(current_image, brightness_values[i])
        enhanced_image.save('saved_photo_cam_enhanced.jpg')
        #enhanced_image.show()
        current_image = 'saved_photo_cam_enhanced.jpg'
        for x,y in faceTop:
            faces[i].append(findNearest(get_pixel_color(current_image,x,y)))

    return faces

def find_missing_color(result):
    present_colors = []
    for i in range(6):
        if i != 3:
            present_colors.append(result[i][4])
    print(present_colors)
    for color in rgbColors:
        if color not in present_colors:
            # Ajoute color à la derniere liste à la 5e position
            result[3].insert(4,color) 
    return result

def find_missing_color2(result):
    rgbColors = ['red', 'orange', 'yellow', 'green', 'blue', 'white']

    # Initialiser le compteur de couleurs
    color_counts = {color: 0 for color in rgbColors}

    # Compter les couleurs dans toutes les listes, y compris la dernière
    for sublist in result:
        for color in sublist:
            if color in rgbColors:
                color_counts[color] += 1
    print(color_counts)

    for color, count in color_counts.items():
        if count == 8:
            # Ajouter cette couleur à la 7ème position de la dernière liste
            result[3].insert(1, color)
            break

    return result


#Parcours les listes de results, ajoute a un string des lettres : U si c'est blanc, R si c'est rouge, B si c'est bleu, F si c'est vert, D si c'est jaune, L si c'est orange

def get_solution(result):
    solution = ''
    for sublist in result:
        for color in sublist:
            match color :
                case 'white':
                    solution += 'D'
                case 'red':
                    solution += 'F'
                case 'blue':
                    solution += 'L'
                case 'green':
                    solution += 'R'
                case 'yellow':
                    solution += 'U'
                case 'orange':
                    solution += 'B'
    return solution

if __name__== "__main__"  :
    faceFront = extraire_couleurs_pures('mask/MaskF.png')
    faceTop = extraire_couleurs_pures('mask/MaskTop.png')
    faceRight = extraire_couleurs_pures('mask/MaskR.png')
    faceLeft = extraire_couleurs_pures('mask/MaskL.png')
    faceBack = extraire_couleurs_pures('mask/MaskB.png')
    faceDown = extraire_couleurs_pures('mask/MaskD.png')

    result = solve(faceTop, faceRight, faceFront, faceDown, faceLeft, faceBack)
    print(result)
    result = find_missing_color(result)
    result = find_missing_color2(result)

    solution = get_solution(result)
    print(kociemba.solve(solution))
