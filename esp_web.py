import requests
import time


# Function to trigger photo capture
def capture_photo(CAPTURE_URL):
    response = requests.get(CAPTURE_URL)


max_attempts=5
# Function to retrieve and save the captured photo
def save_captured_photo(save_path, SAVED_PHOTO_URL):
    attempts = 0

    while attempts < max_attempts:
        try:
            response = requests.get(SAVED_PHOTO_URL)

            if response.status_code == 200:
                # Save the image to the specified path
                with open(save_path, 'wb') as file:
                    file.write(response.content)
                print(f"Photo saved successfully at {save_path}")
                break  # Break out of the loop if successful
            else:
                print(f"Failed to retrieve photo. HTTP status code: {response.status_code}")
        except Exception as e:
            print(f"An error occurred: {str(e)}")

        attempts += 1


def capture_and_save_photo(esp32_ip, esp32_port, save_path):
    # URLs for the different routes
    CAPTURE_URL = f"http://{esp32_ip}:{esp32_port}/capture"
    SAVED_PHOTO_URL = f"http://{esp32_ip}:{esp32_port}/saved-photo"


    # Example usage
    capture_photo(CAPTURE_URL)
    time.sleep(5)
    save_captured_photo(save_path, SAVED_PHOTO_URL)

# Example usage for the server
if __name__ == "__main__":
    cam1_ip = "10.3.141.151"
    cam2_ip = "10.3.141.133"
    esp32_port = 80
    save_path_cam1 = "saved_photo_cam1.jpg"
    save_path_cam2 = "saved_photo_cam2.jpg"
    
    capture_and_save_photo(cam1_ip, esp32_port, save_path_cam1)
    capture_and_save_photo(cam2_ip, esp32_port, save_path_cam2)
