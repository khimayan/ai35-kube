#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2014-2020 Richard Hull and contributors
# See LICENSE.rst for details.
# PYTHON_ARGCOMPLETE_OK

"""
Display the Raspberry Pi logo (loads image as .png).
"""

from pathlib import Path
from demo_opts import get_device
from PIL import Image
import time


def main(device):
    img_path = str(Path(__file__).resolve().parent.joinpath('images', 'pi_logo.png'))
    logo = Image.open(img_path).convert("RGBA")
    fff = Image.new(logo.mode, logo.size, (255,) * 4)

    background = Image.new("RGBA", device.size, "white")
    posn = ((device.width - logo.width) // 2, 0)
    
    rot = logo.rotate(0, resample=Image.BILINEAR)
    img = Image.composite(rot, fff, rot)
    background.paste(img, posn)
    device.display(background.convert(device.mode))

    device.display(background.convert(device.mode))

if __name__ == "__main__":
    try:
        device = get_device()
        main(device)
    except KeyboardInterrupt:
        pass
