import subprocess

def get_pin_status():
    pin_number = 17
    try:
        # Run the pinctrl command and capture the output
        result = subprocess.check_output(['pinctrl', 'get', str(pin_number)], text=True)

        # Check if the result contains 'hi' or 'lo'
        if 'hi' in result:
            return 'hi'
        elif 'lo' in result:
            return 'lo'
        else:
            return None  # Return None for unexpected results
    except subprocess.CalledProcessError as e:
        # Handle any errors or print an error message
        print(f"Error: {e}")
        return None  # Return None in case of an error
