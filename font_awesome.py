#!/usr/bin/env python

from __future__ import unicode_literals

import sys
import random
import time
from pathlib import Path
from PIL import ImageFont

from demo_opts import get_device
from luma.core.render import canvas
from luma.core.sprite_system import framerate_regulator
from button import get_pin_status

def make_font(name, size):
    font_path = str(Path(__file__).resolve().parent.joinpath('fonts', name))
    return ImageFont.truetype(font_path, size)


def oled_logo(device, code, text):

    logo_font = make_font("Monocraft-nerd-fonts-patched.ttf", device.height - 30)
    text_font = make_font("Monocraft-nerd-fonts-patched.ttf", device.height - 50)


    with canvas(device) as draw:
        left, top, right, bottom = draw.textbbox((0, 0), code, logo_font)
        w, h = right - left, bottom - top
        left = (device.width - w) / 2
        top = (device.height - h) - 10
        draw.text((left, top), text=code, font=logo_font, fill="white")

        left, top, right, bottom = draw.textbbox((0, 0), text, text_font)
        w, h = right - left, bottom - top
        left = (device.width - w) / 2
        
        draw.text((left, top), text=text, font=text_font, fill="white")

def oled_result(device, code, text):

    logo_font = make_font("Monocraft-nerd-fonts-patched.ttf", device.height - 30)
    text_font = make_font("Monocraft-nerd-fonts-patched.ttf", device.height - 50)

    turns = text.split(" ")
    # Join each 3 turns with spaces
    joined_turns = [" ".join(turns[i:i + 3]) for i in range(0, len(turns), 3)]
    if turns[len(joined_turns) * 3:] != []:
        ned_turns.append(turns[len(joined_turns) * 3:])
    print(joined_turns)

    screens = ["\n".join(joined_turns[i:i + 3]) for i in range(0, len(joined_turns), 3)]
    if joined_turns[len(screens) * 3:] != []:
        screens.append(joined_turns[len(screens) * 3:])

    for i in range(len(screens)):

        with canvas(device) as draw:
            if(i != len(screens)-1):
                left, top, right, bottom = draw.textbbox((0, 0), code, logo_font)
                w, h = right - left, bottom - top
                left = (device.width - w)
                top = (device.height - h) - 10
                draw.text((left, top), text=code, font=logo_font, fill="white")

            left, top, right, bottom = draw.textbbox((0, 0), text, text_font)
            w, h = right - left, bottom - top

            
            draw.text((left, top), text=screens[i], font=text_font, fill="white")

        while(get_pin_status() != "hi"):
            time.sleep(0.1)


if __name__ == "__main__":
    try:
        device = get_device()
        oled_result(device, "\uEAB6", "R' D2 R' U2 R F2 D B2 U' R F' U R2 D L2 D' B2 R2 B2 U' B2")
        time.sleep(50)
    except KeyboardInterrupt:
        pass
