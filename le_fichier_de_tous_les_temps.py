import color_processing
import cv2
import kociemba

rgbColors = ['red', 'green', 'blue', 'yellow', 'orange', 'white']





def find_missing_color(result):
    present_colors = []
    for i in range(6):
        if i != 3:
            present_colors.append(result[i][4])
    # print(present_colors)
    for color in rgbColors:
        if color not in present_colors:
            # Ajoute color à la derniere liste à la 5e position
            result[3].insert(4, color)
    return result


def find_missing_color2(result):
    # Initialiser le compteur de couleurs
    color_counts = {color: 0 for color in rgbColors}

    # Compter les couleurs dans toutes les listes, y compris la dernière
    for sublist in result:
        for color in sublist:
            if color in rgbColors:
                color_counts[color] += 1
    print(color_counts)

    for color, count in color_counts.items():
        if count == 8:
            # Ajouter cette couleur à la 7ème position de la dernière liste
            result[3].insert(1, color)
            break

    return result

def find_missing_colors(result):
    present_colors = []
    for i in range(6):
        if i != 3:
            present_colors.append(result[i][4])
    # print(present_colors)
    for color in rgbColors:
        if color not in present_colors:
            # Ajoute color à la derniere liste à la 5e position
            result[3].insert(3, color)

    # Initialiser le compteur de couleurs
    color_counts = {color: 0 for color in rgbColors}

    # Compter les couleurs dans toutes les listes, y compris la dernière
    for sublist in result:
        for color in sublist:
            if color in rgbColors:
                color_counts[color] += 1
    print(color_counts)

    for color, count in color_counts.items():
        if count == 8:
            # Ajouter cette couleur à la 7ème position de la dernière liste
            result[3].insert(1, color)
            break

    return result


def get_state_formatted(result):
    solution = ''
    for sublist in result:
        for color in sublist:
            match color:
                case 'white':
                    solution += 'D'
                case 'red':
                    solution += 'F'
                case 'blue':
                    solution += 'L'
                case 'green':
                    solution += 'R'
                case 'yellow':
                    solution += 'U'
                case 'orange':
                    solution += 'B'
    return solution


def solve_le_koob(image_top_path: str, image_bottom_path: str):
    image_top = cv2.imread(image_top_path)
    image_bottom = cv2.imread(image_bottom_path)
    image_top_hsv = cv2.cvtColor(image_top, cv2.COLOR_BGR2HSV)
    image_bottom_hsv = cv2.cvtColor(image_bottom, cv2.COLOR_BGR2HSV)
    colors_lists = color_processing.get_colors(
        image_top_hsv,
        image_bottom_hsv,
    )
    # colors_lists = find_missing_color(colors_lists)
    # colors_lists = find_missing_color2(colors_lists)
    colors_lists = find_missing_colors(colors_lists)

    solution = get_state_formatted(colors_lists)
    return kociemba.solve(solution)
