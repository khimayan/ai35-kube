#!/usr/bin/env python
from demo_opts import get_device
import sys
import time
import threading
import ping3, socket
from esp_web import capture_and_save_photo
from button import get_pin_status

import box_3d
import font_awesome
import pi_logo

cam1_ip = "10.3.141.151"
cam2_ip = "10.3.141.133"

def show_splash_screen(device):
    pi_logo.main(device)
    time.sleep(1)

def is_camera_available(ip):
    try:
        # Performing a verbose ping with the specified count
        ping3.verbose_ping(ip, count=1)

        # Performing a ping and getting the delay
        delay = ping3.ping(ip, timeout=500)

        # If the ping is successful, return True
        return True
    except socket.error as e:
        # If a socket error occurs, print the error and return False
        print(f"Ping Error: {e}")
        return False

def get_pictures():
    esp32_port = 80
    save_path_cam1 = "saved_photo_cam1.jpg"
    save_path_cam2 = "saved_photo_cam2.jpg"
    
    # Create two threads, one for each camera
    thread1 = threading.Thread(target=capture_and_save_photo, args=(cam1_ip, esp32_port, save_path_cam1))
    thread2 = threading.Thread(target=capture_and_save_photo, args=(cam2_ip, esp32_port, save_path_cam2))

    # Start both threads
    thread1.start()
    thread2.start()

    # Wait for both threads to finish
    thread1.join()
    thread2.join()


def main(num_iterations=sys.maxsize):
    device = get_device()

    show_splash_screen(device)

    font_awesome.oled_logo(device, "\ueb34", "Pairing Cams")
    while(not is_camera_available(cam1_ip) or not is_camera_available(cam2_ip)):
        time.sleep(1)

    font_awesome.oled_logo(device, "\uf953", "Press to run")
    while(get_pin_status() != "hi"):
        time.sleep(0.1)

    font_awesome.oled_logo(device, "\uf446", "Taking Pic")
    get_pictures()

    font_awesome.oled_result(device, "\uEAB6","R' D2 R' U2 R F2 D B2 U' R F' U R2 D L2 D' B2 R2 B2 U' B2")

#    font_awesome.oled_logo(device, "\uf27e", "Jessy cook")
    time.sleep(0)

#    font_awesome.oled_logo(device, "\ue235", "Initialisation")
#    time.sleep(2)


#    font_awesome.oled_logo(device, "\uf692", "Saving picture")
#    time.sleep(2)


#    font_awesome.oled_logo(device, "\uf446", "Smile :)")
#    time.sleep(2)

#    font_awesome.oled_logo(device, "\uf499", "Solving")
#    time.sleep(2)

#    font_awesome.oled_logo(device, "\uf554", "Next")
#    time.sleep(2)
'''
    angles = [0, 0, 0]
    start_time = time.time()
    while True:
        box_3d.main(device, angles)

        # Check if 5 seconds have passed
        if time.time() - start_time >= 5:
            break

'''

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
