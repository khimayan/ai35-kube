import cv2
import numpy as np

facelet_to_color = {
    1: np.array([0, 0, 255]),
    2: np.array([0, 255, 0]),
    3: np.array([255, 0, 0]),
    4: np.array([0, 255, 255]),
    5: np.array([255, 0, 255]),
    6: np.array([255, 255, 0]),
    7: np.array([255, 255, 255]),
    8: np.array([1, 1, 1]),
    9: np.array([128, 128, 128]),
}

colors_to_ranges = {
    'red': [{
        'lower': (140, 25, 25),
        'upper': (255, 255, 255),
    }],
    'orange': [{
        'lower': (4, 100, 0),
        'upper': (15, 255, 255),
    }],
    'yellow': [{
        'lower': (20, 100, 25),
        'upper': (40, 255, 255),
    }],
    'green': [{
        'lower': (50, 150, 25),
        'upper': (80, 255, 255),
    }],
    'blue': [{
        'lower': (100, 150, 50),
        'upper': (130, 255, 255),
    }],
    'white': [{  ## Kinda scuffed
        'lower': (50, 0, 110),
        'upper': (180, 150, 255),
    }],
}

DEBUG = False
def get_color_scores(
        face_mask,
        facelet_number,
        hsv_image,
):
    color_scores = {}
    for color, ranges in colors_to_ranges.items():
        for upper_range, lower_range in [(range['upper'], range['lower']) for range in ranges]:
            color_mask = cv2.inRange(hsv_image, lower_range, upper_range)
            facelet_mask = cv2.inRange(face_mask, facelet_to_color[facelet_number], facelet_to_color[facelet_number])
            color_scores[color] = np.sum(color_mask & facelet_mask)
            if DEBUG:
                cv2.imshow(f'color_mask {color}', color_mask & facelet_mask)
    return color_scores



def get_colors(hsv_top, hsv_bottom):
    facelets_masks = {
        'front_face': {
            'image': hsv_top,
            'mask': cv2.imread('NewMasks/rubiks_top_front_face.png'),
        },
        'right_face': {
            'image': hsv_top,
            'mask': cv2.imread('NewMasks/rubiks_top_right_face.png'),
        },
        'top_face': {
            'image': hsv_top,
            'mask': cv2.imread('NewMasks/rubiks_top_top_face.png'),
        },
        'down_face': {
            'image': hsv_bottom,
            'mask': cv2.imread('NewMasks/rubiks_bottom_down_face.png'),
        },
        'left_face': {
            'image': hsv_bottom,
            'mask': cv2.imread('NewMasks/rubiks_bottom_left_face.png'),
        },
        'back_face': {
            'image': hsv_bottom,
            'mask': cv2.imread('NewMasks/rubiks_bottom_back_face.png'),
        },
    }

    color_predictions = {}
    for face_name, face_images in facelets_masks.items():
        face_mask = face_images['mask']
        hsv_image = face_images['image']
        face_colors = []
        for i in range(1, 10):
            color_scores = get_color_scores(face_mask, i, hsv_image)
            face_colors.append(max(color_scores, key=color_scores.get))


        if DEBUG:
            cv2.imshow(f'face mask face {face_name}', face_mask)
            cv2.imshow(f'hsv image', hsv_image)
            while True:
                key = cv2.waitKey(0)
                if key in [27, ord('q'), ord('Q')]:
                    cv2.destroyAllWindows()
                    break

        color_predictions[face_name] = face_colors

    color_predictions['down_face'].pop(4)
    color_predictions['down_face'].pop(1)
    color_predictions_formatted = [
        color_predictions['top_face'],
        color_predictions['right_face'],
        color_predictions['front_face'],
        color_predictions['down_face'],
        color_predictions['left_face'],
        color_predictions['back_face'],
    ]
    return color_predictions_formatted

