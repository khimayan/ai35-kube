#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2014-18 Richard Hull and contributors
# See LICENSE.rst for details.
# PYTHON_ARGCOMPLETE_OK

"""
Rotating 3D box wireframe & color dithering.

Adapted from:
http://codentronix.com/2011/05/12/rotating-3d-cube-using-python-and-pygame/
"""

import sys
import math
from operator import itemgetter
from demo_opts import get_device
from luma.core.render import canvas
from luma.core.sprite_system import framerate_regulator
import numpy as np


def radians(degrees):
    return degrees * math.pi / 180


class point(object):

    def __init__(self, x, y, z):
        self.coords = (x, y, z)
        self.xy = (x, y)
        self.z = z

    def rotate_x(self, angle):
        rad = np.radians(angle)
        rotation_matrix = np.array([[1, 0, 0], [0, np.cos(rad), -np.sin(rad)], [0, np.sin(rad), np.cos(rad)]])
        rotated_coords = np.dot(rotation_matrix, self.coords)
        return point(*rotated_coords)


    def rotate_y(self, angle):
        rad = np.radians(angle)
        rotation_matrix = np.array([[np.cos(rad), 0, np.sin(rad)], [0, 1, 0], [-np.sin(rad), 0, np.cos(rad)]])
        rotated_coords = np.dot(rotation_matrix, self.coords)
        return point(*rotated_coords)


    def rotate_z(self, angle):
        rad = np.radians(angle)
        rotation_matrix = np.array([[np.cos(rad), -np.sin(rad), 0], [np.sin(rad), np.cos(rad), 0], [0, 0, 1]])
        rotated_coords = np.dot(rotation_matrix, self.coords)
        return point(*rotated_coords)

        
    def project(self, size, fov, viewer_distance):
        x, y, z = self.coords
        factor = fov / (viewer_distance + z)
        x_proj = x * factor + size[0] / 2
        y_proj = -y * factor + size[1] / 2
        return point(x_proj, y_proj, z)


def sine_wave(min, max, step=1):
    angle = 0
    diff = max - min
    diff2 = diff / 2
    offset = min + diff2
    while True:
        yield angle, offset + math.sin(radians(angle)) * diff2
        angle += step

def main(device, angles):

    num_iterations=sys.maxsize


    regulator = framerate_regulator(fps=30)

    vertices = [point(-1, 1, -1), point(1, 1, -1), point(1, -1, -1), point(-1, -1, -1),
                point(-1, 1, 1), point(1, 1, 1), point(1, -1, 1), point(-1, -1, 1)]

    faces = [
        (0, 1, 2, 3),
        (1, 5, 6, 2),
        (0, 4, 5, 1),
        (5, 4, 7, 6),
        (4, 0, 3, 7),
        (3, 2, 6, 7)
    ]

    for angle, dist in sine_wave(8, 40, 1.5):
        with regulator:
            num_iterations -= 1
            if num_iterations == 0:
                break

            t = [v.rotate_x(angles[0]).rotate_y(angles[1]).rotate_z(angles[2]).project(device.size, 256, dist)
                                    for v in vertices]

            depth = ((idx, sum(t[vi].z for vi in face) / 4.0) for idx, face in enumerate(faces))

            with canvas(device, dither=True) as draw:
                for idx, avg_z in sorted(depth, key=itemgetter(1), reverse=True)[3:]:
                    (v1, v2, v3, v4) = faces[idx]
                    draw.polygon(sum((t[vi].xy for vi in faces[idx]), ()), None, "white")

            angles[0] += 0.3
            angles[1] -= 5.1
            angles[2] += 0.85

            break


if __name__ == "__main__":
    try:
        device = get_device()

        angles = [0, 0, 0]

        main(device, angles)
    except KeyboardInterrupt:
        pass
